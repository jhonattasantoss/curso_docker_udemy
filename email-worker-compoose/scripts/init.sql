create database email_sender;

\c email_sender

create table emails(
    id serial NOT NULL,
    data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    assunto VARCHAR(100) NOT NULL,
    mensagem VARCHAR(255) NOT NULL
)